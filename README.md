# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary --> Learn opengles2 and create a small game engine
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Tasks ###

* Rework Object Loader, especially load normals
* create texture shader with basic lightning
* Check all Object3D classes, some of them are not finished yet
* Add functions to graphmanager to zoom, and translate on graph
* add function to graph manager to set proper graph width and height

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact