package com.example.nicolas.myopengl.glstuff;

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.example.nicolas.myopengl.ObjectLoader;

import java.nio.FloatBuffer;

/**
 * Created by nicolas on 06/01/2016.
 */
public class TexturedObject extends DrawableObject {

    private int mTextureDataHandle;
    int mProgramHandle;
    int mPositionHandle;
    int mTextureCoordinateHandle;
    int mMVPMatrixHandle;
    private int mTextureUniformHandle;

    FloatBuffer mTextureBuffer;
    //FloatBuffer mTextureIndexBuffer;

    float[] mMVPMatrix = new float[16];


    public TexturedObject(int objRes, int txtRes, GLHelper glHelper)
    {
        super(objRes,glHelper); //without texture

        mTextureBuffer = ObjectLoader.getTextureBuffer();

        mTextureDataHandle = mGLHelper.loadTexture(txtRes);


    }

    public TexturedObject(int txtRes, GLHelper glHelper){
        super(glHelper, txtRes);

        mTextureBuffer = ObjectLoader.getHeightTextBuffer();

        mTextureDataHandle = mGLHelper.loadTexture(txtRes);
    }

    @Override
    public void draw(float[] viewMatrix, float[] projMatrix)
    {

        //rotate(0.5f,0,1,0);

        GLES20.glUseProgram(mProgramHandle);

        // position info
        GLES20.glVertexAttribPointer(mPositionHandle,
                3, GLES20.GL_FLOAT, false,
                0, mVertexBuffer);

        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Set the active texture unit to texture unit 0.
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

        // Bind the texture to this unit.
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle);

        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
        GLES20.glUniform1i(mTextureUniformHandle, 0);


        mTextureBuffer.position(0);
        GLES20.glVertexAttribPointer(mTextureCoordinateHandle, 2, GLES20.GL_FLOAT, false,
                0, mTextureBuffer);

        GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);

        // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
        // (which currently contains model * view).
        Matrix.multiplyMM(mMVPMatrix, 0, viewMatrix, 0, mModelMatrix, 0);

        // This multiplies the modelview matrix by the projection matrix, and stores the result in the MVP matrix
        // (which now contains model * view * projection).
        Matrix.multiplyMM(mMVPMatrix, 0, projMatrix, 0, mMVPMatrix, 0);

        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);

        //GLES20.glEnable(GLES20.GL_CULL_FACE);
        //GLES20.glCullFace(GLES20.GL_BACK);

        //Hide the hidden surfaces using these APIs
        //GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        //GLES20.glDepthFunc(GLES20.GL_LESS);

        //GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, coordsBuffer.limit());
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, mVertexBuffer.capacity()/3);


        //Log.d("CUBE2", "draw called");
    }

    @Override
    public void bindAttributes()
    {
        Log.d("CUBE2", "bind attr");

        mProgramHandle = mGLHelper.getProgramHandle(GLHelper.Program_Type.TEXTURE);
        GLES20.glBindAttribLocation(mProgramHandle, 2, "a_Position");
        GLES20.glBindAttribLocation(mProgramHandle, 3, "a_TexCoordinate");

        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_MVPMatrix");
        mPositionHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Position");

        mTextureCoordinateHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_TexCoordinate");
        mTextureUniformHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_Texture");

        Log.d("CUBE2","end bind attr\n");
    }

}
