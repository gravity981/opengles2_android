package com.example.nicolas.myopengl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.LinkedList;

/**
 * Created by nicolas on 22/12/2015.
 */
public class ObjectLoader {
    private static Context context = null;

    private static float[] vertexArr = null;
    private static float[] textureArr = null;
    private static int[] indexArr = null;
    private static float[] heightArr = null;
    private static float[] heightTexArr;

    private static int mStride = 0;
    private static float[] heightMapVertices;
    private static int[] heightMapIndexData;

    public static void setContext(Context context){
        ObjectLoader.context = context;
    }

    public static boolean readFile(int resID){
        if(context != null) {
            InputStream in = context.getResources().openRawResource(resID);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line = null;
            LinkedList<Float> vertices = new LinkedList<>();
            LinkedList<Float> texturecoords = new LinkedList<>();
            LinkedList<Integer> v_indices = new LinkedList<>();
            LinkedList<Integer> t_indices = new LinkedList<>();
            try {
                while ((line = br.readLine()) != null) {
                    //read letter
                    String[] params = line.split(" ");
                    int color_index = 0;
                    //if (params.length == 4) {
                        if (params[0].equals("v"))
                        {

                            float x = Float.parseFloat(params[1]);
                            float y = Float.parseFloat(params[2]);
                            float z = Float.parseFloat(params[3]);
                            //Log.d("MONKEY", "coords: x: " + x + " y: " + y + " z: " + z);
                            //put in array with x,y,z,r,g,b,a,...
                            vertices.add(x);
                            vertices.add(y);
                            vertices.add(z);
                        }
                        else if (params[0].equals("vt"))
                        {
                            float u = Float.parseFloat(params[1]);
                            float v = Float.parseFloat(params[2]);

                            texturecoords.add(u);
                            texturecoords.add(v);
                            //Log.d("CUBE2", "text coords");
                        }
                        else if (params[0].equals("f"))
                        {
                            String str = "draw order: ";
                            boolean first = true;
                            for (int i = 1; i < params.length; i++) {
                                boolean textureEnabled = false;
                                String f_index[];

                                if(params[i].contains("//"))
                                {
                                    f_index = params[i].split("//");
                                }
                                else if(params[i].contains("/"))
                                {
                                    f_index = params[i].split("/");
                                    textureEnabled = true;

                                }
                                else
                                    f_index = params[i].split(" ");


                                if (first) {
                                    first = false;
                                } else
                                    str += "-->";
                                str += f_index[0];

                                v_indices.add(Integer.parseInt(f_index[0]) - 1);

                                if(textureEnabled)
                                    t_indices.add(Integer.parseInt(f_index[1]) - 1);

                            }
                            //Log.d("MONKEY", str);
                        }
                     else {
                        //Log.d("MONKEY", "unused line");
                    }
                }
                br.close();

                //create coordinate array
                vertexArr = new float[vertices.size()];
                for (int i = 0; i < vertexArr.length; i++) {
                    vertexArr[i] = vertices.get(i);
                }
                Log.d("CUBE2", "coords size: " + vertexArr.length);
                LinkedList<Float> completevertices = new LinkedList<>();
                for(Integer i : v_indices){
                    completevertices.add(vertexArr[i * 3]);    //x
                    completevertices.add(vertexArr[i*3 + 1]);  //y
                    completevertices.add(vertexArr[i*3 + 2]);  //z
                    //Log.d("CUBE2","complete coords: index: " + (i+1) + " / x: " + vertexArr[i * 3] + " / y: " + vertexArr[i * 3 + 1] + " / z: " + vertexArr[i * 3 + 2]);
                }
                vertexArr = new float[completevertices.size()];
                for (int i = 0; i < vertexArr.length; i++) {
                    vertexArr[i] = completevertices.get(i);
                }
                Log.d("CUBE2", "complete coords size: " + vertexArr.length);

                //create draw order array
                indexArr = new int[v_indices.size()];
                for (int i = 0; i < indexArr.length; i++) {
                    indexArr[i] = v_indices.get(i);
                }
                Log.d("CUBE2", "order size: " + indexArr.length);

                //texture coords array
                textureArr = new float[texturecoords.size()];
                for (int i = 0; i < textureArr.length; i++) {
                    textureArr[i] = texturecoords.get(i);
                }
                LinkedList<Float> completeTxtCoords = new LinkedList<>();
                for(Integer i : t_indices){
                    completeTxtCoords.add(textureArr[i*2]);             //u
                    completeTxtCoords.add(1.0f - textureArr[i*2 + 1]);  //v
                }
                textureArr = new float[completeTxtCoords.size()];
                for (int i = 0; i < textureArr.length; i++) {
                    textureArr[i] = completeTxtCoords.get(i);
                }
                Log.d("CUBE2","texture coords len: " + textureArr.length);

                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        //context is null
        return false;

    }

    /**
     *
     * @param resID
     * @return
     *
     * @deprecated use {@link #loadHeightMap(int)} instead
     */
    public static boolean readHeightMap(int resID){
        if(context != null){
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;   // No pre-scaling
            final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resID, options);
            LinkedList<Float> heightmap = new LinkedList<>();
            LinkedList<Float> heightmap_texture = new LinkedList<>();

            int width = bitmap.getWidth();
            int height = bitmap.getHeight();

            //heightArr = new float[(width -1)*(height -1)*2*3*3];
            Log.d("CUBE2","size: " + ((width -1)*(height -1)*2*3*3));

            for(int x = 0; x < width - 1; x++){
                for(int y = 0; y < height - 1; y++){


                    //add face 1
                    //add vertex 1
                    heightmap.add((float) x);
                    heightmap.add((float)y);
                    Log.d("CUBE2", "1: x: " + x + " / y: " + y + " / z: " + getMean(bitmap.getPixel(x, y)));
                    heightmap.add(getMean(bitmap.getPixel(x, y)));
                    //add texture coord
                    heightmap_texture.add(((float)x)/width);
                    heightmap_texture.add(((float)y)/height);

                    //add vertex 2
                    heightmap.add((float)x+1);
                    heightmap.add((float)y);
                    Log.d("CUBE2", "2: x: " + (x+1) + " / y: " + y + " / z: " +getMean(bitmap.getPixel(x+1, y)));
                    heightmap.add(getMean(bitmap.getPixel(x+1,y)));
                    //add texture coord
                    heightmap_texture.add(((float)x+1)/width);
                    heightmap_texture.add(((float)y)/height);

                    //add vertex 3
                    heightmap.add((float)x);
                    heightmap.add((float)y+1);
                    Log.d("CUBE2", "3: x: " + x + " / y: " + (y+1) + " / z: " +getMean(bitmap.getPixel(x, y+1)));
                    heightmap.add(getMean(bitmap.getPixel(x,y+1)));
                    //add texture coord
                    heightmap_texture.add(((float)x)/width);
                    heightmap_texture.add(((float)y+1)/height);

                    //add face 2
                    //add vertex 1
                    heightmap.add((float)x+1);
                    heightmap.add((float)y);
                    heightmap.add(getMean(bitmap.getPixel(x,y)));
                    //add texture coord
                    heightmap_texture.add(((float)x)/width);
                    heightmap_texture.add(((float)y)/height);

                    //add vertex 2
                    heightmap.add((float)x+1);
                    heightmap.add((float)y+1);
                    heightmap.add(getMean(bitmap.getPixel(x+1,y+1)));
                    //add texture coord
                    heightmap_texture.add(((float)x+1)/width);
                    heightmap_texture.add(((float)y+1)/height);

                    //add vertex 3
                    heightmap.add((float)x);
                    heightmap.add((float)y+1);
                    heightmap.add(getMean(bitmap.getPixel(x,y+1)));
                    //add texture coord
                    heightmap_texture.add(((float)x)/width);
                    heightmap_texture.add(((float)y+1)/height);

                    //todo flip texture y coord?

                    //Log.d("HEIGHTMAP","mean: " + mean + " / r: " + red + " / g: " + green + " / b: " + blue);

                }
            }

            heightArr = new float[heightmap.size()];
            for (int i = 0; i < heightArr.length; i++) {
                heightArr[i] = heightmap.get(i);
            }
            //reorder height array, create triangles
            /*LinkedList<Float> complete_heightmap = new LinkedList<>();
            for(int x = 0; x < width - 1; x++){
                for(int y = 0; y < height - 1; y++){
                    complete_heightmap.add(heightArr[x/3]);
                    complete_heightmap.add(heightArr[x/3 + 1]);
                    complete_heightmap.add(heightArr[x/3 + 2]);
                }
            }*/

            heightTexArr = new float[heightmap_texture.size()];
            for (int i = 0; i < heightTexArr.length; i++) {
                heightTexArr[i] = heightmap_texture.get(i);
            }
            
            return true;

        }
        return false;
    }

    public static boolean loadHeightMap(int resID){
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;   // No pre-scaling
        final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resID, options);

        if(bitmap == null)
            return false;

        int xLength = bitmap.getWidth();
        int yLength = bitmap.getHeight();

        //create vertices
        heightMapVertices = new float[bitmap.getWidth()* bitmap.getHeight() * (3 + 2)];
        int index = 0;
        for(int x = 0; x < bitmap.getWidth();x++){
            for(int y = 0; y < bitmap.getHeight(); y++){
                heightMapVertices[index++] = x;
                heightMapVertices[index++] = y;
                heightMapVertices[index++] = getMean(bitmap.getPixel(x,y));

                //add uvs
                heightMapVertices[index++] = (float)x/xLength;
                heightMapVertices[index++] = (float)y/yLength;

            }
        }

        // Now build the index data
        final int numStripsRequired = yLength - 1;
        final int numDegensRequired = 2 * (numStripsRequired - 1);
        final int verticesPerStrip = 2 * xLength;

        heightMapIndexData = new int[(verticesPerStrip * numStripsRequired)
                + numDegensRequired];

        int offset = 0;

        for (int y = 0; y < yLength - 1; y++) {      if (y > 0) {
            // Degenerate begin: repeat first vertex
            heightMapIndexData[offset++] =  (y * yLength);
        }

            for (int x = 0; x < xLength; x++) {
                // One part of the strip
                heightMapIndexData[offset++] = ((y * yLength) + x);
                heightMapIndexData[offset++] =  (((y + 1) * yLength) + x);
            }

            if (y < yLength - 2) {
                // Degenerate end: repeat last vertex
                heightMapIndexData[offset++] =  (((y + 1) * yLength) + (xLength - 1));
            }
        }

        int indexCount = heightMapIndexData.length;

        mStride = (3+2) * 4;

        Log.d("heightmap","vertices count:" + heightMapVertices.length/3 + " / index count: " + indexCount);

        return true;

    }

    private static float getMean(int argb_pixel){
        int alpha = (argb_pixel >> 24) & 0xFF;

        int red = (argb_pixel >> 16) & 0xFF;
        int green = (argb_pixel >> 8) & 0xFF;
        int blue = (argb_pixel & 0xFF);

        float mean = red + green + blue;

        mean /= 3; //calc mean

        //height should be proportional to byte value
        mean /= 255; //max byte value
        mean *= 25; //max height!

        return mean;
    }

    public static FloatBuffer getVertexBuffer()
    {
        FloatBuffer vertexBuffer = ByteBuffer.allocateDirect(vertexArr.length * 4) //4 byte per float
                .order(ByteOrder.nativeOrder()).asFloatBuffer();

        vertexBuffer.put(vertexArr).position(0);

        Log.d("CUBE2","FloatBuffer vertex, capacity: " + vertexBuffer.capacity());

        return vertexBuffer;
    }

    public static FloatBuffer getHeightBuffer()
    {
        FloatBuffer heightBuffer = ByteBuffer.allocateDirect(heightArr.length * 4) //4 byte per float
                .order(ByteOrder.nativeOrder()).asFloatBuffer();

        heightBuffer.put(heightArr).position(0);

        Log.d("CUBE2","FloatBuffer height coords, capacity: " + heightBuffer.capacity());

        return heightBuffer;
    }

    /**
     *
     * @return a buffer
     *
     * @deprecated
     */
    public static FloatBuffer getHeightTextBuffer()
    {
        FloatBuffer heightBuffer = ByteBuffer.allocateDirect(heightTexArr.length * 4) //4 byte per float
                .order(ByteOrder.nativeOrder()).asFloatBuffer();

        heightBuffer.put(heightTexArr).position(0);

        Log.d("CUBE2","FloatBuffer height text, capacity: " + heightBuffer.capacity());

        return heightBuffer;
    }

    public static FloatBuffer getHeightMapBuffer(){

        return allocateFloatBuffer(heightMapVertices);
    }

    public static IntBuffer getHeightMapIndexBuffer(){
        return allocIntBuffer(heightMapIndexData);
    }

    public static IntBuffer getVertexIndexBuffer()
    {

        IntBuffer vIndexBuffer = ByteBuffer.allocateDirect(indexArr.length * 4) //4 byte per int
                .order(ByteOrder.nativeOrder()).asIntBuffer();

        vIndexBuffer.put(indexArr).position(0);

        return vIndexBuffer;
    }

    public static FloatBuffer getTextureBuffer()
    {
        FloatBuffer textureBuffer = ByteBuffer.allocateDirect(textureArr.length * 4) //4 byte per float
                .order(ByteOrder.nativeOrder()).asFloatBuffer();

        textureBuffer.put(textureArr).position(0);

        return textureBuffer.duplicate();
    }

    public static FloatBuffer createColorPlane(){


        //plane
        float vertices[] = {
                0.0f,0.0f,0.0f,
                1.0f,0.0f,0.0f,
                0.0f,1.0f,0.0f,
                1.0f,1.0f,0.0f
        };

        //colors
        float colors[] = {
                0.0f,0.0f,1.0f,1.0f,
                1.0f,1.0f,0.0f,1.0f,
                1.0f,0.0f,0.0f,1.0f,
                0.0f,1.0f,0.0f,1.0f,
        };

        //triangles counter-clockwise

        LinkedList<Float> buffer = new LinkedList<>();
        //tr1
        buffer.add(vertices[0*3+0]);
        buffer.add(vertices[0*3+1]);
        buffer.add(vertices[0*3+2]);
        buffer.add(colors[0*4+0]);
        buffer.add(colors[0*4+1]);
        buffer.add(colors[0*4+2]);
        buffer.add(colors[0*4+3]);

        buffer.add(vertices[1*3+0]);
        buffer.add(vertices[1*3+1]);
        buffer.add(vertices[1*3+2]);
        buffer.add(colors[1*4+0]);
        buffer.add(colors[1*4+1]);
        buffer.add(colors[1*4+2]);
        buffer.add(colors[1*4+3]);

        buffer.add(vertices[2*3+0]);
        buffer.add(vertices[2*3+1]);
        buffer.add(vertices[2*3+2]);
        buffer.add(colors[3*4+0]);
        buffer.add(colors[3*4+1]);
        buffer.add(colors[3*4+2]);
        buffer.add(colors[3*4+3]);

        //tr2
        buffer.add(vertices[1*3+0]);
        buffer.add(vertices[1*3+1]);
        buffer.add(vertices[1*3+2]);
        buffer.add(colors[1*4+0]);
        buffer.add(colors[1*4+1]);
        buffer.add(colors[1*4+2]);
        buffer.add(colors[1*4+3]);

        buffer.add(vertices[3*3+0]);
        buffer.add(vertices[3*3+1]);
        buffer.add(vertices[3*3+2]);
        buffer.add(colors[2*4+0]);
        buffer.add(colors[2*4+1]);
        buffer.add(colors[2*4+2]);
        buffer.add(colors[2*4+3]);

        buffer.add(vertices[2*3+0]);
        buffer.add(vertices[2*3+1]);
        buffer.add(vertices[2*3+2]);
        buffer.add(colors[3*4+0]);
        buffer.add(colors[3*4+1]);
        buffer.add(colors[3*4+2]);
        buffer.add(colors[3*4+3]);

        FloatBuffer fb = allocateFloatBuffer(list2Arr(buffer));

        mStride = (3 + 4)*4; // + 4

        return fb;

    }

    public static int getStride(){
        return mStride;
    }

    private static float[] list2Arr(LinkedList<Float> list){
        float arr[] = new float[list.size()];
        for(int i = 0; i < arr.length; i++){
            arr[i] = list.get(i);
        }
        return arr;
    }
    private static int[] list2ArrI(LinkedList<Integer> list){
        int arr[] = new int[list.size()];
        for(int i = 0; i < arr.length; i++){
            arr[i] = list.get(i);
        }
        return arr;
    }

    private static FloatBuffer allocateFloatBuffer(float[] arr){
        FloatBuffer buffer = ByteBuffer.allocateDirect(arr.length * 4) //4 byte per float
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        buffer.put(arr).position(0);

        return buffer;
    }

    private static IntBuffer allocIntBuffer(int[] arr){
        IntBuffer buffer = ByteBuffer.allocateDirect(arr.length * 4) //4 byte per int
                .order(ByteOrder.nativeOrder()).asIntBuffer();

        buffer.put(arr).position(0);

        return buffer;
    }





    private static class Vertex{
        public float x;
        public float y;
        public float z;
    }
}
