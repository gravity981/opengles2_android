package com.example.nicolas.myopengl.glstuff.newversion;

import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by nicolas on 11/01/2016.
 */
public abstract class Object3D {

    protected FloatBuffer mBuffer;
    protected float mModelMatrix[];
    protected int mStride;

    public Object3D(){
        mModelMatrix = new float[16];
        Matrix.setIdentityM(mModelMatrix,0);
        mStride = 0;
        mBuffer = null;
    }

    public FloatBuffer getBuffer(){
        return mBuffer;
    }

    public float[] getModelMatrix(){
        return mModelMatrix;
    }

    public int getStride(){
        return mStride;
    }

    public void translate(float x, float y, float z){
        Matrix.translateM(mModelMatrix, 0, x, y, z);
    }

    public void rotate(float angle, float xpart, float ypart, float zpart){
        Matrix.rotateM(mModelMatrix, 0, angle, xpart, ypart, zpart);
    }

    public void scale(float xscale, float yscale, float zscale){
        Matrix.scaleM(mModelMatrix, 0, xscale, yscale, zscale);
    }

    public void scale(float scale){
        scale(scale, scale, scale);
    }


}
