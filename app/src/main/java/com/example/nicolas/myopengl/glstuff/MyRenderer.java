package com.example.nicolas.myopengl.glstuff;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import com.example.nicolas.myopengl.R;
import com.example.nicolas.myopengl.glstuff.newversion.ColorObject3D;
import com.example.nicolas.myopengl.glstuff.newversion.Object3D;
import com.example.nicolas.myopengl.glstuff.newversion.TerraObject3D;

import java.util.ArrayList;
import java.util.LinkedList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Nicolas on 21.12.2015.
 */

public class MyRenderer implements GLSurfaceView.Renderer {

    private final Context mContext;
    private final GLHelper mGLHelper;
    private  boolean isDone;
    ArrayList<DrawableObject> drawableObjects;
    LinkedList<Object3D> colorObjects;

    /** How many bytes per float. */
    private final int mBytesPerFloat = 4;
    private final int mBytesPerInt = 4;

    /**
     * Store the view matrix. This can be thought of as our camera. This matrix transforms world space to eye space;
     * it positions things relative to our eye.
     */
    private float[] mViewMatrix = new float[16];

    private float[] mViewMatrixOrig = new float[16];

    /** Store the projection matrix. This is used to project the scene onto a 2D viewport. */
    private float[] mProjectionMatrix = new float[16];

    /**
     * Store the model matrix. This matrix is used to move models from object space (where each model can be thought
     * of being located at the center of the universe) to world space.
     */
    private float[] mModelMatrix = new float[16];

    /** This will be used to pass in the transformation matrix. */
    private int mMVPMatrixHandle;

    /** This will be used to pass in model position information. */
    private int mPositionHandle;

    /** This will be used to pass in model color information. */
    private int mColorHandle;

    /** Allocate storage for the final combined matrix. This will be passed into the shader program. */
    private float[] mMVPMatrix = new float[16];

    /** How many elements per vertex. */
    private  final int mStrideBytes = 0;//7 * mBytesPerFloat;

    /** Offset of the position data. */
    private  int mPositionOffset = 0;

    /** Size of the position data in elements. */
    private int COORDS_PER_VERTEX = 3;

    /** Offset of the color data. */
    private  int mColorOffset = 3;

    /** Size of the color data in elements. */
    private final int mColorDataSize = 4;

    private  float[] color = {0.8f, 0.57f, 0.33f, 1.0f,
        1.0f, 0.0f, 0.0f, 1.0f
    };
    private int mTextureDataHandle;
    private int mProgramHandle;
    private int mTextureCoordinateHandle;
    private int mTextureUniformHandle;

    public MyRenderer(Context context){
       drawableObjects = new ArrayList<>();
        colorObjects = new LinkedList<>();
       mContext = context;
        isDone = false;
        mGLHelper = new GLHelper(context);
    }


    @Override
    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {

        if(!isDone) {
            isDone = true;
            // Set the background clear color to gray.
            GLES20.glClearColor(0.5f, 0.5f, 0.5f, 0.5f);

            // Position the eye behind the origin.
            final float eyeX = 20.0f;
            final float eyeY = 20.0f;
            final float eyeZ = 20.0f;

            // We are looking toward the distance
            //pitch
            final float lookX = 0.0f;
            final float lookY = 0.0f;
            final float lookZ = -10.0f;

            // Set our up vector. This is where our head would be pointing were we holding the camera.
            //tilt
            final float upX = 0.0f;
            final float upY = 0.0f;
            final float upZ = 1.0f;

            // Set the view matrix. This matrix can be said to represent the camera position.
            // NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
            // view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
            Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
            //Matrix.translateM(mViewMatrix,0,-38.4f,-0.0f,0);

            mGLHelper.createAndLinkShaders();

            mTextureDataHandle = mGLHelper.loadTexture(R.drawable.map_texture);


            /*ColorObject3D plane = new ColorObject3D(R.raw.plane);
            plane.translate(-1.0f,0,0);
            ColorObject3D plane2 = new ColorObject3D(R.raw.plane);*/
            TerraObject3D terra = new TerraObject3D(R.drawable.heightmap_256x256);
            terra.translate(-38.4f, -38.4f, 0);
            terra.scale(0.3f);
            //plane.setProgramHandle(mGLHelper.getProgramHandle(GLHelper.Program_Type.COLOR));

            mProgramHandle = mGLHelper.getProgramHandle(GLHelper.Program_Type.TEXTURE);
            GLES20.glUseProgram(mProgramHandle);
            GLES20.glBindAttribLocation(mProgramHandle, 0, "a_Position");
            //GLES20.glBindAttribLocation(mProgramHandle, 1, "a_Color");

            mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_MVPMatrix");
            mPositionHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Position");
           // mColorHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_Color");

            mTextureCoordinateHandle = GLES20.glGetAttribLocation(mProgramHandle, "a_TexCoordinate");
            mTextureUniformHandle = GLES20.glGetUniformLocation(mProgramHandle, "u_Texture");

            GLES20.glEnable(GLES20.GL_DEPTH_TEST);
            GLES20.glDepthFunc(GLES20.GL_LESS);

            /*colorObjects.add(plane);
            colorObjects.add(plane2);*/
            colorObjects.add(terra);

            //Matrix.rotateM(mViewMatrix, 0, -60f, 1, 0, 0);

        }

    }

    @Override
    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
        // Set the OpenGL viewport to the same size as the surface.
        GLES20.glViewport(0, 0, width, height);

        // Create a new perspective projection matrix. The height will stay the same
        // while the width will vary as per aspect ratio.
        final float ratio = (float) width / height;
        final float left = -ratio;
        final float right = ratio;
        final float bottom = -1.0f;
        final float top = 1.0f;
        final float near = 1.0f;
        final float far = 100.0f;

        Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);

        for(int i = 0; i < mViewMatrixOrig.length; i++){
            mViewMatrixOrig[i] = mViewMatrix[i];
        }
    }

    @Override
    public void onDrawFrame(GL10 glUnused) {
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);

        Matrix.rotateM(mViewMatrix,0,0.2f,0,0,1.0f);

        for(Object3D cobj: colorObjects){
            //Log.d("objects","size: " + colorObjects.size() + " / stride: " + cobj.getStride() + " / capacity: " + cobj.getBuffer().capacity());
            if (cobj instanceof ColorObject3D)
                drawColorObjects((ColorObject3D)cobj, mViewMatrix, mProjectionMatrix);
            else if(cobj instanceof TerraObject3D)
                drawTerrain((TerraObject3D)cobj, mViewMatrix, mProjectionMatrix);
        }
    }

    private void drawTerrain(TerraObject3D cobj, float[] viewMatrix, float[] projMatrix) {
        //Log.d("terra","blbais");
        cobj.getBuffer().position(0);
        GLES20.glVertexAttribPointer(mPositionHandle,
                3, GLES20.GL_FLOAT, false,
                cobj.getStride(), cobj.getBuffer());

        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Set the active texture unit to texture unit 0.
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

        // Bind the texture to this unit.
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandle);

        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
        GLES20.glUniform1i(mTextureUniformHandle, 0);


        cobj.getBuffer().position(3);
        GLES20.glVertexAttribPointer(mTextureCoordinateHandle, 2, GLES20.GL_FLOAT, false,
                cobj.getStride(), cobj.getBuffer());

        GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);

        // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
        // (which currently contains model * view).
        Matrix.multiplyMM(mMVPMatrix, 0, viewMatrix, 0, cobj.getModelMatrix(), 0);

        // This multiplies the modelview matrix by the projection matrix, and stores the result in the MVP matrix
        // (which now contains model * view * projection).
        Matrix.multiplyMM(mMVPMatrix, 0, projMatrix, 0, mMVPMatrix, 0);

        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);


        GLES20.glDrawElements(GLES20.GL_TRIANGLE_STRIP, cobj.getIndices().capacity(), GLES20.GL_UNSIGNED_INT, cobj.getIndices());


    }

    private void drawColorObjects(ColorObject3D cobj, float[] viewMatrix, float[] projMatrix){
        //rotate(0.5f,0,1,0);

        //GLES20.glUseProgram(cobj.getProgramHandle());

        // position info
        cobj.getBuffer().position(0);
        GLES20.glVertexAttribPointer(mPositionHandle,
                3, GLES20.GL_FLOAT, false,
                cobj.getStride(), cobj.getBuffer());

        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Pass in the color information

        //there must be as many colors  as vertices!!!!!
        cobj.getBuffer().position(3);
        GLES20.glVertexAttribPointer(mColorHandle, 4, GLES20.GL_FLOAT, false,
                cobj.getStride(), cobj.getBuffer());

        GLES20.glEnableVertexAttribArray(mColorHandle);

        // This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
        // (which currently contains model * view).
        Matrix.multiplyMM(mMVPMatrix, 0, viewMatrix, 0, cobj.getModelMatrix(), 0);

        // This multiplies the modelview matrix by the projection matrix, and stores the result in the MVP matrix
        // (which now contains model * view * projection).
        Matrix.multiplyMM(mMVPMatrix, 0, projMatrix, 0, mMVPMatrix, 0);

        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);

        //GLES20.glEnable(GLES20.GL_CULL_FACE);
        //GLES20.glCullFace(GLES20.GL_BACK);

        //Hide the hidden surfaces using these APIs
        //GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        //GLES20.glDepthFunc(GLES20.GL_LESS);

        //Log.d("CUBE2","draw " + (mVertexBuffer.capacity() / 3));
        //GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, coordsBuffer.limit());
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, cobj.getBuffer().capacity() / (cobj.getStride()/4)); //number of indices!!! not number of coordinates /4 because float and not byte!

        Log.d("CUBE2", "vertex data count: " + cobj.getBuffer().capacity() / (3+4));
    }
}


