package com.example.nicolas.myopengl.glstuff.newversion;

import com.example.nicolas.myopengl.ObjectLoader;
import com.example.nicolas.myopengl.R;

import java.nio.IntBuffer;

/**
 * Created by nicolas on 11/01/2016.
 */
public class TerraObject3D extends Object3D {

    private IntBuffer mIndexBuffer;

    public TerraObject3D(int resID){
        super();

        //create trianglestrip
        ObjectLoader.loadHeightMap(resID);
        mBuffer = ObjectLoader.getHeightMapBuffer();
        mIndexBuffer = ObjectLoader.getHeightMapIndexBuffer();
        mStride = ObjectLoader.getStride();
    }

    public IntBuffer getIndices(){
        return mIndexBuffer;
    }
}
