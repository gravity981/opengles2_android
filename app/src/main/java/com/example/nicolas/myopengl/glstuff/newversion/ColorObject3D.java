package com.example.nicolas.myopengl.glstuff.newversion;

import com.example.nicolas.myopengl.ObjectLoader;

import java.nio.FloatBuffer;

/**
 * Created by nicolas on 08/01/2016.
 */
public class ColorObject3D extends Object3D{

    public ColorObject3D(int resourceID){
        super();

        //TODO implement generation of model using object loader

        //load object with resource
        //ObjectLoader.readFile(resourceID);

        mBuffer = ObjectLoader.createColorPlane();
        mStride = ObjectLoader.getStride();

    }
}
