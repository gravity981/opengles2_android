package com.example.nicolas.myopengl.glstuff;

import android.opengl.Matrix;
import android.util.Log;

import com.example.nicolas.myopengl.ObjectLoader;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Created by nicolas on 06/01/2016.
 */
public abstract class DrawableObject
{


    protected final GLHelper mGLHelper;
    FloatBuffer mVertexBuffer;
    //IntBuffer mVertexIndexBuffer;

    float[] mModelMatrix = new float[16];


    public DrawableObject(int objRes, GLHelper glHelper)
    {
        //load object file and create buffers
        ObjectLoader.readFile(objRes);
    
        mGLHelper = glHelper;
        mVertexBuffer = ObjectLoader.getVertexBuffer();
        Log.d("CUBE2", "vertex buf size: " + mVertexBuffer.capacity());
        //mVertexIndexBuffer = ObjectLoader.getVertexIndexBuffer();

        resetModel();
    }

    public DrawableObject(GLHelper glHelper, int txtRes){
        Log.d("CUBE2", "read heightmap");
        ObjectLoader.readHeightMap(txtRes);
        mGLHelper = glHelper;
        mVertexBuffer = ObjectLoader.getHeightBuffer();
    }

    public abstract void draw(float[] viewMatrix, float[] projMatrix);

    public abstract void bindAttributes();

    public void resetModel(){
        Matrix.setIdentityM(mModelMatrix, 0);
    }

    public void translate(float x, float y, float z){
        Matrix.translateM(mModelMatrix, 0, x, y, z);
    }

    public void rotate(float angle, float xpart, float ypart, float zpart){
        Matrix.rotateM(mModelMatrix, 0, angle, xpart, ypart, zpart);
    }

    public void scale(float xscale, float yscale, float zscale){
        Matrix.scaleM(mModelMatrix, 0, xscale, yscale, zscale);
    }

    public void scale(float scale){
        scale(scale, scale, scale);
    }


}
