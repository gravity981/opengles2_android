package com.example.nicolas.myopengl.glstuff;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

import com.example.nicolas.myopengl.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by nicolas on 06/01/2016.
 */
public class GLHelper
{
    private Context context = null;

    public static enum Program_Type{COLOR, TEXTURE};

    private int programHandle[] = new int[2];

    private boolean programsLinked = false;

    public void setContext(Context c){
        context = c;
        Log.d("CUBE2","SET GLHELPER CONTEXT");
    }

    public GLHelper(Context context){
        this.context = context;
    }

    public void createAndLinkShaders(){
        if(context != null)
        {
            String vertexShaderColor = readTextFileFromRawResource(context, R.raw.vertexshadercolor);
            String fragmentShaderColor = readTextFileFromRawResource(context, R.raw.fragmentshadercolor);
            String vertexShaderTexture = readTextFileFromRawResource(context, R.raw.vertexshadertexture);
            String fragmentShaderTexture = readTextFileFromRawResource(context, R.raw.fragmentshadertexture);

            int col_vertexShaderHandle = compileShader(vertexShaderColor, GLES20.GL_VERTEX_SHADER);
            int tex_vertexShaderHandle = compileShader(vertexShaderTexture, GLES20.GL_VERTEX_SHADER);

            int col_fragmentShaderHandle = compileShader(fragmentShaderColor, GLES20.GL_FRAGMENT_SHADER);
            int tex_fragmentShaderHandle = compileShader(fragmentShaderTexture, GLES20.GL_FRAGMENT_SHADER);

            programHandle[0] = linkShaders(col_vertexShaderHandle, col_fragmentShaderHandle);
            programHandle[1] = linkShaders(tex_vertexShaderHandle, tex_fragmentShaderHandle);

            programsLinked = true;
        }
        else {
            Log.d("CUBE2", "context is null");
            throw new RuntimeException("cannot create shaders, context and resources are null");
        }
    }

    /*public static void useProgram(Program_Type t)
    {
        //Log.d("CUBE2","is linked? " + programsLinked);
        if(programsLinked) {
            switch (t) {
                case COLOR:
                    GLES20.glUseProgram(programHandle[0]);
                    break;
                case TEXTURE:
                    GLES20.glUseProgram(programHandle[1]);
                    break;

            }
        }
        else
            throw new RuntimeException("programs not linked.");
    }*/

    public int getProgramHandle(Program_Type t)
    {
        if(programsLinked) {
            switch (t) {
                case COLOR:
                    return programHandle[0];

                case TEXTURE:
                    return programHandle[1];
            }
        }
        throw new RuntimeException("programs not linked.");
    }

    public int loadTexture(final int resourceId)
    {
        if(context != null) {
            final int[] textureHandle = new int[1];

            GLES20.glGenTextures(1, textureHandle, 0);

            if (textureHandle[0] != 0) {
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;   // No pre-scaling

                // Read in the resource
                final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);
                if(bitmap == null)
                    Log.d("CUBE2","texture was null");
                else
                    Log.d("CUBE2","texture is ok");

                // Bind to the texture in OpenGL
                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

                // Set filtering
                GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
                GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

                // Load the bitmap into the bound texture.
                GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

                // Recycle the bitmap, since its data has been loaded into OpenGL.
                bitmap.recycle();
            }

            if (textureHandle[0] == 0) {
                throw new RuntimeException("Error loading texture.");
            }

            return textureHandle[0];
        }
        throw new RuntimeException("Context not set in load texture.");
    }

    //returns shader handle
    private static int compileShader(String shader, int type)
    {
        int shaderhandle = GLES20.glCreateShader(type);

        if (shaderhandle != 0)
        {
            // Pass in the shader source.
            GLES20.glShaderSource(shaderhandle, shader);

            // Compile the shader.
            GLES20.glCompileShader(shaderhandle);

            // Get the compilation status.
            final int[] compileStatus = new int[1];
            GLES20.glGetShaderiv(shaderhandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

            // If the compilation failed, delete the shader.
            if (compileStatus[0] == 0)
            {
                GLES20.glDeleteShader(shaderhandle);
                shaderhandle = 0;
            }
        }

        if (shaderhandle == 0)
        {
            throw new RuntimeException("Error creating shader.");
        }

        return shaderhandle;
    }

    private static int linkShaders(int vertexShaderHandle, int fragmentShaderHandle)
    {
        // Create a program object and store the handle to it.
        int programHandle = GLES20.glCreateProgram();

        if (programHandle != 0)
        {
            // Bind the vertex shader to the program.
            GLES20.glAttachShader(programHandle, vertexShaderHandle);

            // Bind the fragment shader to the program.
            GLES20.glAttachShader(programHandle, fragmentShaderHandle);

            // Bind attributes
            //GLES20.glBindAttribLocation(programHandle, 0, "a_Position");
            //GLES20.glBindAttribLocation(programHandle, 1, "a_Color");

            // Link the two shaders together into a program.
            GLES20.glLinkProgram(programHandle);

            // Get the link status.
            final int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

            // If the link failed, delete the program.
            if (linkStatus[0] == 0)
            {
                GLES20.glDeleteProgram(programHandle);
                programHandle = 0;
            }
        }

        if (programHandle == 0)
        {
            throw new RuntimeException("Error creating program.");
        }

        return programHandle;
    }


    private static String readTextFileFromRawResource(final Context context,
                                                     final int resourceId)
    {
        if(context != null) {
            final InputStream inputStream = context.getResources().openRawResource(
                    resourceId);
            final InputStreamReader inputStreamReader = new InputStreamReader(
                    inputStream);
            final BufferedReader bufferedReader = new BufferedReader(
                    inputStreamReader);

            String nextLine;
            final StringBuilder body = new StringBuilder();

            try {
                while ((nextLine = bufferedReader.readLine()) != null) {
                    body.append(nextLine);
                    body.append('\n');
                }
            } catch (IOException e) {
                return null;
            }

            return body.toString();
        }
        return null;
    }

}
